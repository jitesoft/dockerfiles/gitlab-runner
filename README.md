# Gitlab-ci runner

Image based on the gitlab-runner:alpine image.  
Using env variables to create and register a docker container which works with swarm mode.

Check the gitlab-ci runner env variables at the gitlab-ci docs.

You must define the `REGISTRATION_TOKEN` env variable to be able to register the runner for your group or projects.

Available environment variables can be found [here](https://gitlab.com/jitesoft/dockerfiles/gitlab-runner/snippets/1722665) (`gitlab-runner register --help output`):
