# !/bin/ash

if [ ! -f /etc/gitlab-runner/config.toml ]; then
    echo "Registering gitlab runner."
    touch /etc/gitlab-runner/config.toml
    gitlab-runner register
fi

gitlab-runner verify

echo "Running the runner."
exec /usr/bin/dumb-init /entrypoint run --user=gitlab-runner --working-directory=/home/gitlab-runner