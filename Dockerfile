FROM gitlab/gitlab-runner:alpine

ENV REGISTER_NON_INTERACTIVE="true" \
    RUNNER_REQUEST_CONCURRENCY="4" \
    CHECK_INTERVAL="0" \
    DOCKER_VOLUMES="/var/run/docker.sock:/var/run/docker.sock" \
    RUNNER_NAME="My runner" \
    CI_SERVER_URL="https://gitlab.com" \
    REGISTRATION_TOKEN="" \
    RUNNER_EXECUTOR="docker" \
    DOCKER_IMAGE="alpine:latest" \
    DOCKER_PRIVILEGED="false" \
    DOCKER_DISABLE_CACHE="true" \
    DOCKER_PULL_POLICY="always" \
    DOCKER_SHM_SIZE="0" \
    REGISTER_LOCKED="false" \
    METRICS_SERVER=":9252"

COPY startup.sh /startup.sh
RUN chmod +x /startup.sh \
    && apk add --no-cache curl curl-dev

HEALTHCHECK --interval=2m --timeout=3s CMD curl -f http://localhost:9252/metrics || exit 1
ENTRYPOINT [ "/bin/ash" ]
CMD [ "startup.sh" ]
